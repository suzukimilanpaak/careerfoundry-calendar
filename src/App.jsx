import React from 'react';
import Calendar from './features/calendar/Calendar';
import 'bulma/css/bulma.css';
import './App.scss';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Calendar />
      </header>
    </div>
  );
}

export default App;
