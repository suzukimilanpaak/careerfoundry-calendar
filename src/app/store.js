import { reducer as reduxFormReducer } from 'redux-form';
import { configureStore } from '@reduxjs/toolkit';
import { slotReducer } from '../features/dateCell/slotsSlice';
import dateCellFormReducer from '../features/dateCell/formSlice';
import myModalReducer from '../features/myModal/myModalSlice';

export default configureStore({
  reducer: {
    form: reduxFormReducer,
    slot: slotReducer,
    myModal: myModalReducer,
    dateCellForm: dateCellFormReducer,
  },
});
