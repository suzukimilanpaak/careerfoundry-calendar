import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import Modal from 'react-modal';
import {
  close,
  selectMyModalOpen,
  selectMyModalText,
} from './myModalSlice';

const customStyle = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

Modal.setAppElement('#root');

export default function MyModal({ buttonCaption }) {
  const myModalOpen = useSelector(selectMyModalOpen);
  const text = useSelector(selectMyModalText);
  const dispatch = useDispatch();

  return (
    <div>
      <button
        type="submit"
        className="button is-link"
      >
        {buttonCaption}
      </button>
      <Modal
        isOpen={myModalOpen}
        onRequestClose={() => dispatch(close())}
        style={customStyle}
        contentLabel="Example Modal"
        className=""
      >
        <div className="column">
          {text}
        </div>
        <div className="column">
          <button
            type="submit"
            onClick={() => dispatch(close())}
            className="button is-link"
          >
            OK
          </button>
        </div>
      </Modal>
    </div>
  );
}

MyModal.propTypes = {
  buttonCaption: PropTypes.string.isRequired,
};
