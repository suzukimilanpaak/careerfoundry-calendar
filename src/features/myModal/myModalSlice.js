import { createSlice } from '@reduxjs/toolkit';

export const myModalSlice = createSlice({
  name: 'myModal',
  initialState: {
    text: '',
    opened: false,
  },
  reducers: {
    open: (state, action) => {
      state.text = action.payload;
      state.opened = true;
    },
    close: (state) => {
      state.opened = false;
    },
  },
});

export const { open, close } = myModalSlice.actions;
export const selectMyModalOpen = (state) => state.myModal.opened;
export const selectMyModalText = (state) => state.myModal.text;
export default myModalSlice.reducer;
