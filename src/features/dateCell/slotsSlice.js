import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const saveSlot = createAsyncThunk(
  'slot/post',
  async () => fetch('/recipes'),
);

export const slotSlice = createSlice({
  name: 'slot',
  initialState: {
    slotVisible: false,
    chosenSlot: null,
  },
  reducers: {
    showSlots: (state) => {
      state.slotVisible = true;
    },
    hideSlots: (state) => {
      state.slotVisible = false;
    },
    chooseSlot: (state, action) => {
      state.chosenSlot = action.payload;
    },
  },
  extraReducers: {
    [saveSlot.fulfilled]: () => {},
    [saveSlot.rejected]: () => {},
  },
});

export const { showSlots, hideSlots, chooseSlot } = slotSlice.actions;
export const selectSlotVisible = (state) => state.slot.slotVisible;
export const selectChosenSlot = (state) => state.slot.chosenSlot;
export const slotReducer = slotSlice.reducer;
