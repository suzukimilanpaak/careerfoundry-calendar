import { createSlice } from '@reduxjs/toolkit';

export const formSlice = createSlice({
  name: 'form',
  initialState: {
    reason: '',
  },
  reducers: {
    updateReason: (state, action) => {
      state.reason = action.payload;
    },
  },
});

export const { updateReason } = formSlice.actions;
export const selectReason = (state) => state.dateCellForm.reason;
export default formSlice.reducer;
