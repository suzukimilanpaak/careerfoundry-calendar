import React from 'react';
import { Field } from 'redux-form';
import { useDispatch } from 'react-redux';
import strftime from 'strftime';
import {
  chooseSlot,
} from './slotsSlice';

export default function Slots() {
  const dispatch = useDispatch();
  const responseBody = {
    mentor: {
      name: 'Max Mustermann',
      time_zone: '-03:00',
    },
    calendar: [
      {
        date_time: '2020-05-16 14:00:00 +0000',
      },
      {
        date_time: '2020-05-16 09:00:00 +0000',
      },
      {
        date_time: '2020-08-10 07:00:00 +0000',
      },
      {
        date_time: '2020-09-19 09:00:00 +0000',
      },
      {
        date_time: '2020-05-02 06:00:00 +0000',
      },
    ],
  };

  const calendar = responseBody.calendar.map((slot) => new Date(slot.date_time)).sort();
  const slots = calendar.reduce(
    (sum, slot) => {
      const value = sum[strftime('%F', slot)];
      if (value) {
        sum[strftime('%F', slot)] = [value, slot];
      } else {
        sum[strftime('%F', slot)] = slot;
      }
      return sum;
    },
    {},
  );

  return (
    slots['2020-05-16'].map((slot) => {
      const dateTime = strftime('%H:%M', slot);
      return (
        <div className="control button is-info is-outlined is-fullwidth">
          <label className="raido column" htmlFor={slot.toISOString()}>
            <Field
              name="slot"
              component="input"
              type="radio"
              onClick={() => dispatch(chooseSlot(dateTime))}
              value={slot.toISOString()}
            />
            {strftime('%H:%M', slot)}
          </label>
        </div>
      );
    })
  );
}
