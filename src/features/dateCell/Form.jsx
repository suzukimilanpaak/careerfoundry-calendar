import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { useDispatch } from 'react-redux';
import MyModal from '../myModal/MyModal';
import Slots from './Slots';
import {
  updateReason,
} from './formSlice';

const Form = (props) => {
  const dispatch = useDispatch();
  const { handleSubmit } = props;

  return (
    <form onSubmit={handleSubmit}>
      <div className="buttons">
        <Slots />
      </div>
      <div className="field">
        <Field
          name="reason"
          component="textarea"
          type="text"
          placeholder="Reason For Call"
          onChange={(e) => dispatch(updateReason(e.target.value))}
          className="textarea"
        />
      </div>
      <div>
        <MyModal
          buttonCaption="Confirm Call"
        />
      </div>
    </form>
  );
};

Form.propTypes = {
  handleSubmit: PropTypes.string.isRequired,
};

export default reduxForm({
  form: 'dateCellForm',
})(Form);
