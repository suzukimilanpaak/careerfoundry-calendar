import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import strftime from 'strftime';
import styles from './DateCell.module.scss';
import DateCellForm from './Form';
import {
  saveSlot,
  showSlots,
  hideSlots,
  selectSlotVisible,
} from './slotsSlice';

import {
  open,
} from '../myModal/myModalSlice';

export default function DateCell() {
  const slotVisible = useSelector(selectSlotVisible);
  const dispatch = useDispatch();
  const saveAndConfirm = async (values) => {
    const text = `
      The call was successfully booked for slot at ${strftime('%F %T', new Date(values.slot)) || 'Not Chosen'}.
      Reason - ${values.reason || 'Not Specified'}
    `;
    const resultAction = await dispatch(saveSlot());
    if (saveSlot.fulfilled.match(resultAction)) {
      dispatch(open(text));
    } else {
      dispatch(open('Failed to book a slot: Slot might be already booked for a call.'));
    }
  };

  return (
    <div className="container">
      <div className="container">
        <a
          href="#slot"
          onClick={() => dispatch(slotVisible ? hideSlots() : showSlots())}
        >
          Mon 16 Nov
        </a>
      </div>
      <div className="container">
        <div className={slotVisible ? styles.visible : styles.hidden}>
          <DateCellForm onSubmit={saveAndConfirm} />
        </div>
      </div>
    </div>
  );
}
